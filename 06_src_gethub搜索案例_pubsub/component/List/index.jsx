import React, { Component } from 'react';
import Pubsub from 'pubsub-js';
import './index.css'

export default class List extends Component {

    state = {
        users: [],
        isFirst: true,
        isLoading: false,
        err: ''
    }

    componentDidMount() {
        this.token = Pubsub.subscribe('atguigu', (msg, sateObj) => {
            this.setState(sateObj)
        })
    }

    componentWillUnmount () {
        Pubsub.unsubscribe(this.token)
    }

    render() {
        const {users, isFirst, isLoading, err} = this.state
        return (
            <div className="row">
                {
                    isFirst ? <h2>欢迎使用，输入关键字，随后点击搜索</h2> :
                    isLoading ? <h2>Loading...</h2> : 
                    err ? <h2 stype={{color: 'red'}} >{err}</h2> :
                    users.map((userObj) => {
                        return (
                            <div key={userObj.id} className="card">
                                <a rel="noreferrer" href={userObj.html_url} target="_blank">
                                <img alt="head_portrait" src={userObj.avatar_url} style={{width: '100px'}}/>
                                </a>
                                <p className="card-text">{userObj.login}</p>
                            </div>
                        )
                    })
                }
            </div>
        );
    }
}