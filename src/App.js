// 创建“外壳”组件
import React,{Component} from 'react'
import {DatePicker, Button} from 'antd'
import 'antd/dist/antd.less'
import {WechatOutlined, SearchOutlined} from '@ant-design/icons'

const {RangePicker} = DatePicker;

export default class App extends Component {
    render() {
        return (
            <div>
                App...
                <button>点我</button>
                <Button type="primary">Primary Button</Button>
                <Button>Primary Button</Button>
                <Button type="link">Primary Button</Button>
                <Button type="primary" icon={<SearchOutlined/>}>
                Search
                </Button>
                <WechatOutlined/>
                <DatePicker/>
                <RangePicker/>
            </div>
        )
    }
}