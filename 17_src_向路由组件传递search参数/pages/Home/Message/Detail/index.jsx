import React, { Component} from 'react';
import {useLocation} from 'react-router-dom'
import qs from 'query-string'

const DetailData = [
    {id: '01', content: '你好，中国'},
    {id: '02', content: '你好，尚硅谷'},
    {id: '03', content: '你好，未来的自己'}
]

export default function Detail() {
    const search = useLocation().search.substring(1);
    const result = qs.parse(search)
    const data = DetailData.find( search => search.id === result.id )
    return (
        <div>
            <ul>
                <li>ID: {result.id}</li>
                <li>TITLE: {result.title}</li>
                <li>CONTENT: {data.content}</li>
            </ul>
        </div>  
    )
}