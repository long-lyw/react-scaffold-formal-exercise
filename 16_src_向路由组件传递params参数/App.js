// 创建“外壳”组件
import React,{Component, useEffect} from 'react'
import {Route, Routes, useNavigate} from 'react-router-dom'
import MyNavLink from './components/MyNavLink'
import Home from './pages/Home'
import About from './pages/About' // About是路由组件
import Header from './components/Header' // Header是一般组件

function Redirect({ to }) {
    let navigate = useNavigate();
    useEffect(() => {
      navigate(to);
    });
    return null;
  }

export default class App extends Component {
    render() {
        return (
            <div>
                <div className="row">
                    <div className="col-xs-offset-2 col-xs-8">
                        <Header/>
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-2 col-xs-offset-2">
                        <div className="list-group">
                            <MyNavLink to={`/about`}>About</MyNavLink>
                            <MyNavLink to={`/home`}>Home</MyNavLink>
                        </div>
                    </div>
                    <div className="col-xs-6">
                        <div className="panel">
                            <div className="panel-body">
                                {/* 注册路由 */}
                                <Routes>
                                    <Route path="about" element={<About/>}/>
                                    <Route path="home/*" element={<Home/>}/>
                                    <Route path="" element={<Redirect to="/home"/>}/>
                                </Routes>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}