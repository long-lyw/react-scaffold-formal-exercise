import React, { Component } from 'react';
import Pubsub from 'pubsub-js';
import axios from 'axios';

export default class Search extends Component {

    search = async () => {
        // 获取用户输入
        const {keyWorldElement: {value: keyWorld}} = this
        // this.props.updataAppState({isFirst: false, isLoading: true})
        Pubsub.publish('atguigu', {isFirst: false, isLoading: true})

        try {
            const response = await fetch(`/api1/search/users?q=${keyWorld}`)
            const data = await response.json()
            Pubsub.publish('atguigu', {users: data.items, isLoading: false})
        } catch (err) {
            Pubsub.publish('atguigu', {isLoading: false, err: err.message})
        }
        

    }

    render() {
        return (
            <section className="jumbotron">
                <h3 className="jumbotron-heading">Search Github Users</h3>
                <div>
                    <input ref={c => {this.keyWorldElement = c}} type="text" placeholder="输入关键词点击搜索"/>&nbsp;
                    <button onClick={this.search}>搜索</button>
                </div>
            </section>
        );
    }
}