// 创建“外壳”组件
import React,{Component} from 'react'
import {Route, Routes} from 'react-router-dom'
import Home from './pages/Home'
import About from './pages/About' // About是路由组件
import Header from './components/Header' // Header是一般组件
import MyNavLink from './components/MyNavLink' 
import Test from './components/Test' 

export default class App extends Component {
    render() {
        return (
            <div>
                <div className="row">
                    <div className="col-xs-offset-2 col-xs-8">
                        <Header/>
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-2 col-xs-offset-2">
                        <div className="list-group">
                            <MyNavLink to="/atguigu/about">About</MyNavLink>
                            <MyNavLink to="/atguigu/home">Home</MyNavLink>
                        </div>
                    </div>
                    <div className="col-xs-6">
                        <div className="panel">
                            <div className="panel-body">
                                {/* 注册路由 */}
                                <Routes>
                                    <Route path="/atguigu/about" element={<About/>}/>
                                    <Route path="/atguigu/home" element={<Home/>}/>
                                    <Route path="/atguigu/home" element={<Test/>}/>
                                </Routes>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}