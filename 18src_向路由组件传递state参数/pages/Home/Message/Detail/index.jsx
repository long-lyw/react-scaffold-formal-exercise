import React, { Component} from 'react';
import { useLocation} from 'react-router-dom';
import { createBrowserHistory } from 'history';

const DetailData = [
    {id: '01', content: '你好，中国'},
    {id: '02', content: '你好，尚硅谷'},
    {id: '03', content: '你好，未来的自己'}
]

export default function Detail() {
    let state = null
    if (state = useLocation() !== null) state = createBrowserHistory().location.state
    const data = DetailData.find( stateData => stateData.id === state.id )
    return (
        <div>
            <ul>
                <li>ID: {state.id}</li>
                <li>TITLE: {state.title}</li>
                <li>CONTENT: {data.content}</li>
            </ul>
        </div>  
    )
}