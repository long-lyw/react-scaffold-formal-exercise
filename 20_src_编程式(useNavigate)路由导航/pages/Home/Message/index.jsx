import React, { Component } from 'react';
import {Route, Routes, Link, useNavigate} from 'react-router-dom'
import Detail from './Detail'

export default function Message() {
    const state = {
        messageArr: [
            {id: '01', title: '消息1'},
            {id: '02', title: '消息2'},
            {id: '03', title: '消息3'}
        ]
    }
    const navigate = useNavigate()

    console.log(navigate.prototype)
    function pushRouter(id, title) {
        navigate(`/home/message/detail/${id}/${title}`)
    }
    function replaceRouter(id, title) {
        navigate(`/home/message/detail/${id}/${title}`, {replace: true})
    }
    function fallbackRouter(fallback) {
        navigate(fallback)
    }
    function fallbackRouter(fallback) {
        navigate(fallback)
    }
    function forwardRouter(forward) {
        navigate(forward)
    }
    return (
        <div>
            <ul>
                {
                    state.messageArr.map((msgObj) => {
                        return (
                            <li key={msgObj.id}>
                                <Link to={`/home/message/detail/${msgObj.id}/${msgObj.title}`}>{msgObj.title}</Link>
                                &nbsp;<button onClick={() => pushRouter(msgObj.id, msgObj.title)}>push查看</button>
                                &nbsp;<button onClick={() => replaceRouter(msgObj.id, msgObj.title)}>replace查看</button>
                            </li>
                        )
                    })
                }
            </ul>
            <Routes>
                <Route path="detail/:id/:title" element={<Detail/>}/>
            </Routes>
            <button onClick={() => fallbackRouter(-1)}>回退</button>
            <button onClick={() => forwardRouter(1)}>前进</button>
        </div>
    )
}