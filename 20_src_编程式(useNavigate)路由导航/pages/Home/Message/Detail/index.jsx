import React, { Component} from 'react';
import {useParams} from 'react-router-dom'

export default function Detail() {
    const DetailData = [
        {id: '01', content: '你好，中国'},
        {id: '02', content: '你好，尚硅谷'},
        {id: '03', content: '你好，未来的自己'}
    ]

    const params = useParams();
    const data = DetailData.find( result => result.id === params.id )
    return (
        <div>
            <ul>
                <li>ID: {params.id}</li>
                <li>TITLE: {params.title}</li>
                <li>CONTENT: {data.content}</li>
            </ul>
        </div>  
    )
}