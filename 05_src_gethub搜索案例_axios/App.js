// 创建“外壳”组件
import React,{Component} from 'react'
import Search from './component/Search'
import List from './component/List'

export default class App extends Component {

    state = {
        users: [],
        isFirst: true,
        isLoading: false,
        err: ''
    }

    updataAppState = (stateObj) => {
        this.setState(stateObj)
    }

    render() {
        return (
            <div className="container">
                <Search updataAppState={this.updataAppState}/>
                <List {...this.state}/>
            </div>
        );
    }
}