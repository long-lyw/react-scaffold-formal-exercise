import React, { Component } from 'react';
import axios from 'axios';

export default class Search extends Component {

    search = () => {
        // 获取用户输入
        const {keyWorldElement: {value: keyWorld}} = this
        this.props.updataAppState({isFirst: false, isLoading: true})
        // 发送网络请求
        axios.get(`/api1/search/users?q=${keyWorld}`).then(
            response => {
                this.props.updataAppState({users: response.data.items, isLoading: false})
            },
            error => {
                this.props.updataAppState({isLoading: false, err: error.message})
            }
        )
    }

    render() {
        return (
            <section className="jumbotron">
                <h3 className="jumbotron-heading">Search Github Users</h3>
                <div>
                    <input ref={c => {this.keyWorldElement = c}} type="text" placeholder="输入关键词点击搜索"/>&nbsp;
                    <button onClick={this.search}>搜索</button>
                </div>
            </section>
        );
    }
}