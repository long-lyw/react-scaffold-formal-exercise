import React, { Component } from 'react';
import {useNavigate} from 'react-router-dom'

export default function Header() {
    const navigate = useNavigate()

    function fallbackRouter(fallback) {
        navigate(fallback)
    }
    function forwardRouter(forward) {
        navigate(forward)
    }
    return (
        <div className="page-header">
            <h2>React Router Demo</h2>
            <button onClick={() => fallbackRouter(-1)}>回退</button>
            <button onClick={() => forwardRouter(1)}>前进</button>
        </div>
        
    )
}