import React, { Component, useEffect} from 'react';
import {Route, Routes, useNavigate} from 'react-router-dom'
import MyNavLink from '../../components/MyNavLink'
import News from './News'
import Message from './Message'

function Redirect({ to }) {
    let navigate = useNavigate();
    useEffect(() => {
      navigate(to);
    });
    return null;
  }

export default class Home extends Component {
    render() {
        return (
            <div>
                <h2>我是Home的内容</h2>
                <div>
                    <ul className="nav nav-tabs">
                        <li>
                            <MyNavLink to="/home/news">News</MyNavLink>
                        </li>
                        <li>
                            <MyNavLink to="/home/message">Message</MyNavLink>
                        </li>
                    </ul>
                    <Routes>
                        <Route path="news" element={<News/>}/>
                        <Route path="message/*" element={<Message/>}/>
                        <Route path="" element={<Redirect to="/home/news"/>}/>
                    </Routes>
                </div>
            </div>
            
        );
    }
}